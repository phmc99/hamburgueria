import { useState } from "react";
import "./style.css";
import { MenuContainer } from "./components/MenuContainer";

const state = {
  products: [
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ],
  filteredProducts: [],
  currentSale: { total: 0, saleDetails: [] },
};

function App() {
  const [userInput, setUserInput] = useState("");
  const [produtos, setProdutos] = useState([...state.products]);
  const [cart, setCart] = useState([]);

  const showProducts = (input) => {
    setProdutos(
      produtos.filter(
        (item) => item.name.toLowerCase().indexOf(input.toLowerCase()) > -1
      )
    );
  };

  const handleClick = (productId) => {
    const product = state.products.find((item) => item.id === productId);
    const newCart = [...cart, product];
    setCart(newCart.filter((item, i) => newCart.indexOf(item) === i));
  };

  return (
    <div className="App">
      <form onSubmit={(event) => event.preventDefault()}>
        <input
          type="text"
          value={userInput}
          onChange={(event) => setUserInput(event.target.value)}
        />
        <button onClick={() => showProducts(userInput)}>Buscar</button>
      </form>
      <MenuContainer
        produtos={produtos}
        handleClick={handleClick}
        cart={cart}
      />
    </div>
  );
}

export default App;
