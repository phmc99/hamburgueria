import { Products } from "../Products";
import "./style.css";

export function MenuContainer({ produtos, handleClick, cart }) {
  return (
    <div className="container">
      <ul>
        {produtos.map((item, index) => (
          <li key={index}>
            <Products props={item} handleClick={handleClick} />
          </li>
        ))}
      </ul>
      <ul>
        {cart.map((item, index) => (
          <li key={index}>
            <Products props={item} handleClick={handleClick} hide={true} />
          </li>
        ))}
      </ul>
      <span>
        Subtotal: {cart.reduce((acc, item) => acc + item.price, 0).toFixed(2)}
      </span>
    </div>
  );
}
