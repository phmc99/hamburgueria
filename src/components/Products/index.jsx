import "./style.css";

export function Products({ props, handleClick, hide = false }) {
  return (
    <div className="card-product">
      <h2>{props.name}</h2>
      <h3>{props.category}</h3>
      <h3>{props.price}</h3>
      <button
        onClick={() => handleClick(props.id)}
        className={hide ? "hide" : "no-hide"}
      >
        Adicionar
      </button>
    </div>
  );
}
